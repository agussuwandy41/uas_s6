import React,{useState} from 'react'
import { View, Text ,Image,StyleSheet,KeyboardAvoidingView,TouchableOpacity,Alert,} from 'react-native'
import {TextInput as MaterialTextInput,Button} from 'react-native-paper'
import auth from '@react-native-firebase/auth';

// import firestore from '@react-native-firebase/firestore'
// import messaging from '@react-native-firebase/messaging';

const SignupScreen = ({navigation}) => {

  const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    const userSignup = async () => {
    const result = await auth().signInWithEmailAndPassword(email,password)
      console.log(result.user)
    }

  return (
    <KeyboardAvoidingView style={{flex:1}}  > 
    <View style={styles.container}>
      <View style={styles.container}>
        <View style={styles.box1}>
          <Image style={styles.gambarSplash} source={require('../Assets/imageSplash.png')}/>
          <Text style={styles.text}>please sign up !</Text>
        </View>

        <View style={styles.container1}>
          <View style={{marginVertical:5 }}>
            <MaterialTextInput syle={styles.materialText} 
              placeholder={"Masukkan Email or User Name Anda"}  
              label="Email"
              value={email}
              mode="outlined"
              onChangeText={text => setEmail(text)}
            />
          </View>
          <View style={{marginVertical:25 }}>
            <MaterialTextInput style={styles.textinput}
              placeholder={"Masukkan Password Anda"} 
              label="password"
              value={password}
              mode="outlined"
              secureTextEntry={true}
              onChangeText={text => setPassword(text)}
            />
          </View>
        </View>

        <View style={{marginHorizontal:'5%'}}>
        <Button  mode="contained" onPress={() => userSignup()}>
          Sig nup
        </Button>
        
          <Button onPress={()=>navigation.goBack()} style={styles.touchable}>
            <Text style={{textAlign:"center"}}>Login?</Text>
          </Button>
        </View>
      </View>
    </View>
    </KeyboardAvoidingView>
    
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'#f8f8f8',
    // paddingVertical:15
  },
  container1: {
    margin:'5%',
  },
  gambarSplash: {
    width:180,
    height:180,
  },
  box1: {
    alignItems: 'center',
    justifyContent:'center',
  },
  text: {
    fontSize:24,
    color: '#000', //hitam
    marginTop:'5%',
    fontWeight:'500',
  },
  touchable: {
    marginVertical:7
  }
});