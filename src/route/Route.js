import React,{useState,useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar
} from 'react-native';
import {LoginScreen, SignupScreen, CreateAdScreen, AccountScreen, ListItemScreen,} from '../Screens';
import {DefaultTheme,Provider as PaperProvider} from 'react-native-paper';
// import 'react-native-gesture-handler';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationContainer ,DefaultTheme as DefaultThemeNav} from '@react-navigation/native';
import splashScreens from '../splashScreens/Splash';

// stack navigator
const Stack = createNativeStackNavigator()

const AuthNavigator = ()=>{
  return(
    <Stack.Navigator>
      {/* <Stack.Screen
        name="Splash"
        component={splashScreens}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Beranda"
        component={TabNavigator}
        options={{headerShown:false}}
      /> */}
      <Stack.Screen 
        name="login" 
        component={LoginScreen} 
        options={{headerShown:false}}
      />
      <Stack.Screen 
        name="signup" 
        component={SignupScreen} j7
        options={{headerShown:false}} 
      />
    </Stack.Navigator>
  ); 
};

//button tab 
const Tab = createBottomTabNavigator();

const TabNavigator = ()=>{
  return(
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        let rn = route.name
        
        if (rn === 'Home') {
          iconName = focused
          ? 'home'
          : 'home-outline';
        }else if(rn === 'post'){
          iconName = focused
          ? 'md-file-tray-full' 
        : 'md-file-tray-full-outline';
        }
        else if (rn === 'Account'){
          iconName = focused
          ? 'ios-person' 
        : 'ios-person-outline';
        }

        //pemanggilan icon
        
      return <Ionicons name={iconName} size={25} color={color} />;
      },

      tabBarActiveTintColor: 'deepskyblue',
      tabBarInactiveTintColor: 'gray',
    })}
    >
      
    <Tab.Screen 
      name="Home" 
      component={ListItemScreen} 
      options={{headerShown: false}} 
    />

    <Tab.Screen 
      name="post" 
      component={CreateAdScreen}  
      options={{headerShown: false}} 
    />

    <Tab.Screen 
      name="Account" 
      component={AccountScreen}  
      options={{headerShown: false}} 
    />
  </Tab.Navigator>
  );
};

const Route = () => {

  const user = ""
  return (
    <NavigationContainer>
      {user?<TabNavigator />:<AuthNavigator />} 
      {/* <TabNavigator/> */}
    </NavigationContainer>
  )
}

export default Route

const styles = StyleSheet.create({})